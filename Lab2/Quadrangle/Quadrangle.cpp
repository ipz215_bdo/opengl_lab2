#include <GLFW/glfw3.h> 

int main(void)
{
    GLFWwindow* window;

    /* ������������ �������� */
    if (!glfwInit())
        return -1;

    /* ������� ���� �������� ������ �� ���� �������� OpenGL */
    window = glfwCreateWindow(800, 600, "Baginskiy Dima IPZ-21-5", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* ������� �������� �������� ���� */
    glfwMakeContextCurrent(window);

    /* ����, ���� ���������� �� ����� ���� */
    while (!glfwWindowShouldClose(window))
    {
        /* ������ ��� */
        glClear(GL_COLOR_BUFFER_BIT);

        /*������� ���� ���� �� ��������*/
        glClearColor(3.0f, 1.0f, 0.0f, 1.0f);

        /* ������� ������������� */
		glBegin(GL_QUADS); //������� ��������� �������������
		glColor3f(0.0f, 1.0f, 0.0f); //������ ���� ������ �������������
		glVertex2f(-0.5f, -0.5f); //������ ���������� ������ �������������
		glVertex2f(0.5f, -0.5f);
		glVertex2f(0.5f, 0.5f);
		glVertex2f(-0.5f, 0.5f);
		glEnd(); //ʳ���� ��������� �������������

        /* �������� ������� �� ����� ������ */
        glfwSwapBuffers(window);

        /* ���������� �� ������� ���� */
        glfwPollEvents();
    }
    glfwTerminate();
    return 0;
}